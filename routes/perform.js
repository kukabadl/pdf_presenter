/*
  This router is for users who have administrator privileges over a presentation.
  From here they control the presentaion. To get permission one has to:
    1. provide presentation ID, 
    2. provide presentation SECRET,
    3. provide their cookie. TODO: or uuid.
  One only gets the permission if all of the above information is provided and:
    1. presentation with given ID exists,
    2. the provided SECRET is valid for given presentation,
    3. provided UUID belongs to a user registered under the presentation whose privileges.priv == 'admin'
*/
var express = require('express');
const path = require('path');
const Auth = require('../controllers/auth.js');

var router = express.Router();



var handleAuth = function (req, res) {
  var status = Auth.simple(req.params.presId, req.params.presSecret);
  if(status.authorized) res.render('perf-spect', { role: 'perform' });
  else res.status(404).sendFile(path.join(__dirname, '../public/not_found.html'));
}


router.get('/', (req, res) => {
  return res.status(404).sendFile(path.join(__dirname, '../public/not_found.html'));
});


//Check if presentation exists. If so, render perform.
router.get('/:presId/:presSecret', handleAuth);
module.exports = router;
// const Auth = require('../controllers/auth.js');
const express = require('express');
let s = require('../controllers/search');
var router = express.Router();



function searchAndRender(req, res) {
    if (req.query && req.query.search) {
        let results = s.search(req.query.search);
        res.sendStatus(200);
    }
    else res.sendStatus(404);
}

function searchAndJson(req, res) {
    if (req.query && req.query.search) {
        let results = s.search(req.query.search);
        (results) ? res.json(results) : res.sendStatus(404);
        return;
    }
    res.sendStatus(404);
}



router.get('/', searchAndRender);
router.get('/suggest', searchAndJson);
module.exports = router;
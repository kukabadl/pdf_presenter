const { body, validationResult } = require('express-validator');
const User = require('../controllers/user.js');
const Presentation = require('../controllers/presentation.js');
const express = require('express');
const db = require('../controllers/db_access/mongo_azure')
const path = require('path');
const sanFname = require('sanitize-filename');
const { oneOf } = require('express-validator');

//For each checkbox you need to add the input element's name to this array
//    AND
//Add it's corresponding validation line e.g. 
//  body(checkboxIds[N]).trim().escape(),
const checkboxIds = ['public-performance', 'chat-for-all', 'viewer-control', 'viewer-preview', 'viewer-voice', 'custom-id-enable'];
let checkExt = {};
var router = express.Router();

function secretGen(length = 15) {
  let chars = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
  let str = '';
  for (let i = 0; i < length; i++) str += chars.charAt(Math.floor(Math.random() * chars.length));
  return str;
}

function checkIdAvail(customId) {
  let pres = Presentation.findPresById(customId);
  if (pres == undefined) return true;
  throw new Error('It must be unique. This one is already in use.');
}

function isValidId(value) {
  let whitelist = /^([\w-]+)$/g;
  console.log(`Checking ${value}`)
  if (whitelist.test(value)) return true;
  throw new Error("Custom id contains forbidden characters. You can only use alphanumerical and hyphens.")
}

router.post('/upload', [
  body(checkboxIds[0]).trim().escape().toBoolean(true).custom((value, { path }) => { checkExt[path] = value; return true;}),
  body(checkboxIds[1]).trim().escape().toBoolean(true).custom((value, { path }) => { checkExt[path] = value; return true;}),
  body(checkboxIds[2]).trim().escape().toBoolean(true).custom((value, { path }) => { checkExt[path] = value; return true;}),
  body(checkboxIds[3]).trim().escape().toBoolean(true).custom((value, { path }) => { checkExt[path] = value; return true;}),
  body(checkboxIds[4]).trim().escape().toBoolean(true).custom((value, { path }) => { checkExt[path] = value; return true;}),
  body(checkboxIds[5]).trim().escape().toBoolean(true).custom((value, { path }) => { checkExt[path] = value; return true;}),
  oneOf([
    //if custom id disabled
    body('custom-id', "Custom id must be valid and available when enabled.").custom((value) => {
      return checkExt['custom-id-enable'] === false;
    }),
    //or if id is valid and available
    body('custom-id').trim().escape().isLength({ min: 5, max: 25 })
      .withMessage('It is too short or too long (length must be between 5 and 25)').bail().custom(isValidId).bail().custom(checkIdAvail)
  ]),
  body('performers-name', 'Name is too short (minimum length is 3)').trim().isLength({ min: 3 }).escape(),
  body('pres-name', 'Name is too short (minimum length is 3)').trim().isLength({ min: 3 }),

], async (req, res) => {
  try {

    var submit = { status: true, err: {} };
    const err = validationResult(req);
    console.log(JSON.stringify(err, null, '\t'));

    //extract true/false from checkboxes
    let extractedCheckboxes = {};
    checkboxIds.forEach((val) => {
      extractedCheckboxes[val] = Boolean(req.body[val]);
      //console.log(`Key: ${val}, Value: ${extractedCheckboxes[val]}`);
    });

    err.errors.forEach(function (val) {
      if (val.nestedErrors) {
        val.nestedErrors.forEach(er => {
          if (submit.err[er.param] === undefined) submit.err[er.param] = "";
          submit.err[er.param] += er.msg + ' ';
        });
      }
      else {
        if (submit.err[val.param] === undefined) submit.err[val.param] = "";
        submit.err[val.param] += val.msg + ' ';
      }
    });

    if (!req.files) {
      submit.err['pdf-file'] = 'Presentation not uploaded';
    }

    if (Object.keys(submit.err).length == 0) {
      //All inputs are ok, create presentation
      let fl = req.files['pdf-file'];
      let filename = secretGen() + sanFname(fl.name);

      let pa = path.join(__dirname, '../uploads/', filename);
      console.log(`Saving the uploaded file as ${pa}.`)
      fl.mv(pa);

      let defPriv = User.defaultPrivileges;
      defPriv.chat = extractedCheckboxes['chat-for-all'];
      defPriv.control = [extractedCheckboxes['viewer-control']];
      defPriv.indivPace.custom = extractedCheckboxes['viewer-preview'];
      defPriv.voice = extractedCheckboxes['viewer-voice'];

      //let presHost = new User(User.adminPrivileges, null, req.body['performers-name']);
      let presParams = {
        public: extractedCheckboxes['public-performance'],
        fName: filename,
        presName: req.body['pres-name'],
        customId: (extractedCheckboxes['custom-id-enable'] === true) ? req.body['custom-id'] : undefined,
        spectPriv: defPriv
      }
      let pres = new Presentation(presParams);
      let presHost = pres.presHost;

      presHost.name = req.body['performers-name'];

      submit.status = true;
      submit.msg = 'New presentation has been succesfully created.';

      submit.url = `/perform/${pres.id}/${pres.presSecret}`;
      submit.user = { name: presHost.name, uid: presHost.uid, uuid: presHost.uuid, priv: presHost.privileges };
      submit.pres = { id: pres.id, presSecret: pres.presSecret, downURL: `/down/${pres.id}/${pres.presSecret}` };

      console.log(`Submit status:\n${JSON.stringify(submit, null, '\t')}`);
      console.log(`Pres to DB:\n${pres.getDbEntry()}`);
      //console.log(`To the database: ${pres.dbEntry()}`);
      res.status(200);
    }
    else {
      //Something went wrong, send error message
      res.status(400);
    }
    res.send(JSON.stringify(submit));

  } catch (submit) {
    res.status(400).send({ status: false });
  }
});
module.exports = router;
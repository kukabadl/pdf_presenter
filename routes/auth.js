const Auth = require('../controllers/auth.js');
const express = require('express');
var router = express.Router();


var handleAuth = function (req, res) {
    var status = Auth.simple(req.params.presId, req.params.presSecret);
    if(status.authorized) {
        let user = status.pres.newUser();
        let resp = {
            user: { name: user.name, uid: user.uid, uuid: user.uuid, priv: user.privileges },
            pres: { id: status.pres.id }
        }
        res.status(200);
        res.json(resp);
    }
    else res.sendStatus(404);
}

router.get('/:presId', handleAuth);
router.get('/:presId/:presSecret', handleAuth);
module.exports = router;
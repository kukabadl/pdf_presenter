var express = require('express');
const path = require('path');
const Auth = require('../controllers/auth.js');

var router = express.Router();

var handleAuth = function (req, res) {
  var status = Auth.simple(req.params.presId, req.params.presSecret);
  if(status.authorized) res.render('perf-spect', { role: 'spectate' });
  else res.sendStatus(404);
}

/*
  '/'
    Spectate overview is not yet implemented, therefore if you don't specify
    presId, you will get an error message 404 Not found
*/
router.get('/', (req, res) => {
  return res.status(404).sendFile(path.join(__dirname, '../public/not_found.html'));
});

/*
  '/:presId'
    This route takes care of serving public presentations, which anyone can watch.
    When a viewer only specifies presentation's public id and the presentation is only
    available privately they get 404 Not found error message. If their request isn't valid
    they get 400 Bad request error. 
*/
router.get('/:presId', handleAuth);

/*
  '/:presId/:presSecret'
    This route takes care of serving private presentations. Spectator has to specify both
    'presId' and 'presSecret' correctly in order to access a private presentation. 

    In case that the presentation is not actually private checking of the 'presSecret'
    is skipped and the content is served.

    If the request isn't valid 400 Bad request is served. If the ids are not correct
    404 Not found is served.
*/

router.get('/:presId/:presSecret', handleAuth);

module.exports = router;
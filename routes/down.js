const { body, validationResult } = require('express-validator');
const User = require('../controllers/user.js');
const Presentation = require('../controllers/presentation.js');
const express = require('express');
const Auth = require('../controllers/auth.js');
const path = require('path');
var fs = require("fs");
var router = express.Router();

var handleAuth = function (req, res) {
    var status = Auth.simple(req.params.presId, req.params.presSecret);
    if(status.authorized) { 
        //path.resolve  maintain cross platform compatibility.
        //path.normalize This will present you with the correct path on whatever platform you are using.
        //path.join If you need to join paths together use path.join. This will also normalize the result for you.
        let pa = path.join(__dirname, '/../', 'uploads', status.pres.fName);
        console.log(`Reading from ${pa}`);
        console.log(`I'm in ${path.join(__dirname)}`);

        res.sendFile(pa);
    }
    else res.sendStatus(404);
}

router.get('/:presId', handleAuth);
router.get('/:presId/:presSecret', handleAuth);
module.exports = router;

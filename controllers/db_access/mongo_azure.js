//conn.priv.js file exports url string for connection to the mongoDB on Azure
const dbUrl = require('./conn.priv.js');
const Presentation = require('../presentation.js');

var MongoClient = require('mongodb').MongoClient;

MongoClient.connect(dbUrl, function (err, cli) {
    if(err) throw err;
    const db = cli.db("pdf-pres");

    db.collection("pres").find({}).toArray(function (err, result) {
        if(err) throw err;
        Presentation.importPres(result);
        cli.close();
    });
});


const User = require('./user.js');
var MongoClient = require('mongodb').MongoClient;
let search = require('../controllers/search');

class Presentation {
    static all = {};
    static public = new Array();

    static idGen(maxVal = 99999, minVal = 10000) {
        do {
            var id = Math.floor(Math.random() * (maxVal - minVal + 1)) + minVal;
        } while(id in Presentation.all);
        return id;
    }

    static importPres(presFromDb) {
        presFromDb.forEach((pres, idx) => {
            let pr = new Presentation();
            pr.import(pres);
            console.log(`Imported presentation with id ${pr.id} (${pr.presName})`);
        });
        search.init(Presentation.public);
    }

    static findPresById(id) {
        if(id in Presentation.all) return Presentation.all[id];
        return undefined;
    }

    constructor(param) {
        this.usermap = [];
        this.usercount = 0;

        if(param) {
            this.public = (param.public) ? param.public : false;
            this.fName = param.fName;
            this.presName = param.presName;
            this.spectPriv = (param.spectPriv) ? param.spectPriv : User.defaultPrivileges;

            this.presHost = this.admin();
            this.id = (param.customId) ? param.customId: Presentation.idGen();
            this.presSecret = this.secretGen();

            this.url = `/spectate/${this.id}/${this.presSecret}`;
            this.downloadUrl = `/down/${this.id}/${this.presSecret}`;
            Presentation.all[this.id] = this;
            if (this.public === true) {
                Presentation.public.push(this.extractPublic());
                search.add(this.extractPublic());
            }
            this.insertIntoDb();
        }
    }


    import(params = {}) {
        this.public = params.public;
        this.fName = params.fName;
        this.presName = params.presName;
        this.spectPriv = params.spectPriv;

        this.presHost = this.newUser(params.presHost);
        this.id = params.id;
        this.presSecret = params.presSecret;
        this.url = params.url;
        this.downloadUrl = params.downloadUrl;
        Presentation.all[this.id] = this;
        if (this.public === true) Presentation.public.push(this.extractPublic());
    }
    
    insertIntoDb() {
        let pres = this;
        const dbUrl = require('./db_access/conn.priv.js');
        MongoClient.connect(dbUrl, function (err, cli) {
            if(err) throw err;
            const db = cli.db("pdf-pres");
            db.collection('pres').insertOne(pres.getDbEntry()).then(result => {
                console.log(`I attempted to insert a value into the db. See the status for yourself ${JSON.stringify(result)}`);
                cli.close();
            });
        });
    }

    extractPublic(){
        let dat = {
            presId: this.id.toString(),
            presName: this.presName,
            hostName: this.presHost.name
        }
        if (this.desc) dat.presDesc = this.desc;
        if (this.friendlyId) dat.friendlyId = this.friendlyId;
        return dat;
    }

    getDbEntry() {
        let entry = {
            public: this.public,
            fName: this.fName,
            presName: this.presName,
            spectPriv: JSON.parse(JSON.stringify(this.spectPriv)),
            presHost: this.presHost.getDbEntry(),
            id: this.id,
            presSecret: this.presSecret,
            url: this.url,
            downloadUrl: this.downloadUrl
        }
        return entry;
    }

    send(message, sender, recIds = [0]) {
        message.sender = { uid: sender.uid, name: sender.name }
        let msg = JSON.stringify(message);
        if(recIds[0] == 0) {
            this.usermap.forEach(user => {
                if(user.ws && user.uid != sender.uid) user.ws.send(msg);
            });
        }
        else {
            recIds.forEach(uid => {
                let user = this.findUserByUid(uid);
                if(user && user.ws) user.ws.send(msg);
            });
        }
    }

    print() {
        console.log(`\rPresentation name: ${this.presName}
            \rPresentation spectate link: ${this.url}
            \rHost\'s name: ${this.presHost.name} (${this.presHost.uuid})
            \rFile name: ${this.fName}
            \rPresentation\'s id: ${this.id} secret: ${this.presSecret}
            \rIs it public? ${this.public}
            \rDefault privileges:
            ${JSON.stringify(this.spectPriv, null, '\t')}`);
        this.usermap.forEach(user => user.print());
    }

    secretGen(length = 22) {
        let chars = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
        let str = '';
        for(let i = 0; i < length; i++) str += chars.charAt(Math.floor(Math.random() * chars.length));
        return str;
    }

    admin() {
        if(!this.presHost) {
            this.presHost = this.newUser({ privileges: User.adminPrivileges });
        }
        return this.presHost;
    }

    newUser(params = {}) {
        this.usercount += 1;
        let userPars = {};
        userPars.uid = this.usercount;
        userPars.uuid = (params.uuid) ? params.uuid : undefined;
        userPars.name = (params.name) ? params.name : undefined;
        userPars.privileges = (params.privileges) ? params.privileges : this.spectPriv;
        if(userPars.name && this.findUserByName(userPars.name) != undefined) userPars.name = undefined;

        let usr = new User(userPars);
        this.usermap.push(usr);
        return usr;
    }

    removeUserByUid(uid) {
        let userdbIdx = this.getUserIdxInUdb(uid);
        if(userdbIdx != undefined) {
            console.log(`removing:`);
            this.usermap[userdbIdx].print();
            this.usermap[userdbIdx].destroy();
            this.usermap.splice(userdbIdx, 1);
        }
    }

    getUserIdxInUdb(idx) {
        for(let i = 0; i < this.usermap.length; i++) {
            if(this.usermap[i].uid == idx) return i;
        }
        return undefined;
    }

    findUserByUuid(uuid) {
        for(let i = 0; i < this.usermap.length; i++) {
            if(this.usermap[i].uuid == uuid) return this.usermap[i];
        };
        return undefined;
    }

    findUserByUid(uid) {
        if(uid != undefined) {
            for(let i = 0; i < this.usermap.length; i++) {
                if(this.usermap[i].uid == uid) return this.usermap[i];
            }
        }
        return undefined;
    }

    findUserByName(name) {
        for(let i = 0; i < this.usermap.length; i++) {
            if(this.usermap[i].name == name) return this.usermap[i];
        };
        return undefined;
    }
}

module.exports = Presentation;
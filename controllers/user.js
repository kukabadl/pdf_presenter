const { v4: uuidv4 } = require('uuid');

class User {
    static all = {};
    static listPrivileges = { chat: [true, false], indivPace: { next: true, prev: true, custom: true }, control: ['switch', 'kick'], role: [false, 'admin'], voice: [true, false] };
    static adminPrivileges = { chat: true, indivPace: { next: true, prev: true, custom: true }, control: [true, 'switch', 'point', 'kick'], role: 'admin', voice: true };
    static defaultPrivileges = { chat: true, indivPace: { next: 0, prev: 0, custom: false }, control: [], role: false, voice: false };
    static nonePrivileges = { chat: false, indivPace: { next: 0, prev: 0, custom: false }, control: [], role: false, voice: false };

    constructor(params) {
        this.uuid = (params.uuid) ? params.uuid : uuidv4();
        this.uid = params.uid;
        this.name = params.name;
        this.privileges = JSON.parse(JSON.stringify(params.privileges));

        this.ws = null;
        User.all[this.uuid] = this;
    }

    getDbEntry(){
        let copy = {
            uuid: String(this.uuid),
            uid: this.uid,
            name: this.name,
            privileges: JSON.parse(JSON.stringify(this.privileges)),
        };
        return copy;
    }

    destroy() {
        this.ws.close();
    }

    print() {
        console.log(`User ${this.name} (${this.uid} ->> ${this.uuid})\nPrivileges: ${JSON.stringify(this.privileges, null, '\t')}`);
    }
}
module.exports = User;
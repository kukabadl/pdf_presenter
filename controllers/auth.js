const Pres = require('./presentation.js');

class Auth{
    constructor(){
        this.pres = Pres;
    }
    cookie(cook, verbose = true){
        let status = {authorized: false};
        let presentation = Pres.findPresById(cook.pres.id);
        if(presentation && cook.pres.presSecret == presentation.presSecret){
            let user = presentation.findUserByUuid(cook.uuid);
            if(user && (!verbose ||(user.privileges.priv === 'admin'))){
                status.pres = presentation;
                status.user = user;
                status.authorized = true;
                status.msg = `User ${cook.uuid} was successfully authorised.`;
            }
        }
        else status.msg = "Presentation was not found";
        return status;
    }

    simple(presId, presSecret = undefined){
        let status = {authorized: false};
        let presentation = Pres.findPresById(presId);
        if(presentation && (presentation.public || presentation.presSecret === presSecret)){
            status.authorized = true;
            status.pres = presentation;
        }
        else status.msg = "Presentation was not found";
        return status;
    }

    admin(presId, presSecret, uuid){
        let status = {authorized: false};
        let presentation = Pres.findPresById(presId);
        if(presentation && presentation.presSecret == presSecret){
            let user = presentation.presHost;
            if(user && (user.privileges.role === 'admin')){
                status.pres = presentation;
                status.user = user;
                status.authorized = true;
                status.msg = `User ${uuid} was successfully authorized.`;
            }
        }
        else status.msg = "Presentation was not found";
        return status;
    }

    auth(presId = 0, uid = 0, uuid = ''){
        let status = {authorized: false};
        let presentation = Pres.findPresById(presId);
        if(presentation){
            var user = presentation.findUserByUid(uid);
            if(user && user.uuid == uuid){
                status.pres = presentation;
                status.user = user;
                status.authorized = true;
                status.msg = `User ${uuid} was successfully authorised.`;
            }
        }
        else status.msg = "Presentation was not found";
        return status;
    }

    ws(cred = {}){
        if(cred.pres && cred.user){
            return this.auth(cred.pres.id, cred.user.uid, cred.user.uuid);
        }
    }
}

var auth = new Auth();
module.exports = auth
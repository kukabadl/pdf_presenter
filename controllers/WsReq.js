const ReqStat = require('./ReqStat.js');    //(code, msg = '', display = 'silent')
var validator = require('validator');

let Check = {
    msg: function(danger, ws){
        try{
            if(ws.user.privileges.chat){
                var toBeSent = validator.escape(danger.content);
                toBeSent = validator.trim(toBeSent);
                if(toBeSent != ''){
                    let out_msg = {msg: {text: toBeSent}};
                    ws.pres.send(out_msg, ws.user, danger.rec);
                }
                else throw new ReqStat('err', `I will not send empty messages.`);
            }
            else throw new ReqStat('err', `You do not have permission to send messages.`, 'loud');    
        }
        catch(err){
            if(err instanceof ReqStat) throw err;
            else{
                console.log(`${JSON.stringify(err)}`);
                throw new ReqStat('err', `Your syntax was probably wrong. Sending messages failed.`, 'loud');
            }
        }
    },

    ctl: function (danger, ws) {
        //['switch', 'kick', 'point'];
        try {
            if(danger.cmd == 'switch') {
                let page = parseInt(danger.param);
                if(ws.user.privileges.control.includes('switch') || ws.user.privileges.control[0] === true) {
                    if(page != NaN) {
                        ws.pres.send({ ctl: { cmd: 'switch', param: page }}, ws.user);
                    }
                    else throw new ReqStat('err', 'Specified pageNumber is not a number.');
                }
                else throw new ReqStat('err', 'You do NOT have permission to do this.', 'loud');
            }
            else if(danger.cmd == 'point' || ws.user.privileges.control[0] === true) {
                /*
                    danger.param = {
                        t0: 213141254,
                        time: [2, 44, 2, 22, 9],
                        x: [123, 376, 999, 0, 444],
                        y: [123, 376, 999, 0, 444]
                    }
                */
                if(ws.user.privileges.control.includes('point') || ws.user.privileges.control[0] === true) {
                    if(danger.param && danger.param.t0 && danger.param.time && danger.param.x && danger.param.y) {
                        ws.pres.send({ ctl: { cmd: 'point', param: danger.param } }, ws.user);
                    }
                    else throw new ReqStat('err', 'Invalid parameters.');
                }
                else throw new ReqStat('err', 'You do NOT have permission to do this.', 'loud');
            }
            else if(danger.cmd == 'kick') {
                if(ws.user.privileges.control.includes('kick') || ws.user.privileges.control[0] === true)
                console.log(`Got ${danger.cmd}`);
                throw new ReqStat('err', 'Feature is not ready yet.', 'silent');
            }
            else throw new ReqStat('err', 'Feature is not implemented.', 'silent');

        }
        catch(err) {
            if(err instanceof ReqStat) throw err;
            else {
                console.log(`${JSON.stringify(err)}`);
                throw new ReqStat('err', `Your syntax was probably wrong. Sending command failed.`, 'silent');
            }
        }
    },

    cleanse: function(danger){
        for(const par of danger){
            if(typeof(danger[par]) === 'string'){
                danger[par] = validator.escape(danger[par]);
            }
        }
        return danger;
    },

    pres: function (attrs, data){
        console.log(`Checking ${JSON.stringify(data)}`);
    },

    def: function (attrs, data){
    }
}


/*
let example = {
    //Only required parameter is cred
    cred: {
        user: {name: 'username', uid: 12, uuid: '157fe1a1-31a3-4bbf-8c14-a14ffeb06684'},
        pres: {id: '10001'},
    },
    msg: {rec: [1, 3, 42], content: 'Hello, I've just joined. Can somebody, please tell me why are there so many people here?'},
    ctl: [
        {cmd: 'next', param: null},
        {cmd: 'switch', param: 45},
        {cmd: 'kick', param: [1, 23, 34, 4, 21]}
}
*/
class WsReq{
    static all = {
        msg: {attributes: ['rec', 'content'], func: Check.msg},
        ctl: {attributes: [['cmd', 'param'], ['cmd', 'param'], ['cmd', 'param']], func: Check.ctl},
    }
    static params = Object.keys(WsReq.all);

    do(ws_in, msg){
        //Sanitize and validate, before parsing to JSON
        let danger = JSON.parse(msg);
        WsReq.params.forEach(val => {
            //Perform all actions on valid data
            if(danger[val]){
                //Property exists
                WsReq.all[val].func(danger[val], ws_in);
            }
        });
 
    }
}

var ws_req = new WsReq();
module.exports = ws_req;
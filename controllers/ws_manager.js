let WebSocketServer = require('ws').Server
const httpServer = require('../controllers/http-server.js');
var validator = require('validator');
const ReqStat = require('./ReqStat.js');    //(code, msg = '', display = 'silent')
const WsReq = require('./WsReq.js');
const Auth = require('./auth.js');

/*
    websocket communication structure
    1.  Connection establishment
        - User connects to the server. The time when the connection was established along with the websocket object are automatically appended to impostorWs. 
            If the client doesn't authenticate in 6 seconds after the connection establishement, the connection is closed.
        - Authenticating the user for the first time requires the client to provide all required credentials, namely: id of the presentation, uid and uuid which must match against their record
            inside the presentation class. If not, they are denied and not removed from impostorWs which results in their disconnection. 
        - If authentication succeeds, the ws connection is assigned 2 new properties - user and pres. After that authentication is accelerated by just checking if ws.user is defined.
        - If the above procedure succeedes newly connecter users get removed from impostorWs.
    2.  Sending Data to others
        - Any user can send data to the server once the connection was opened. Server then only chooses parameter that ore already predefined. Any custom data will be discarded.
        - After that the server checks what the user privileges are and further eliminates those fields that the user is not allowed to send.
        - The selected data gets then sanitized and optionally validated and brocasted to the recipient(s) or an action is exetuted if the data is more of a command than a message.
*/

let wsServer = new WebSocketServer({
    noServer: true, 
    perMessageDeflate: false
});
//let wsServer = new WebSocketServer({ port: 8000 });
console.log("WebSocket server is listening on port 8000.");
var impostorWs = [];

function printWss(val, idx, arr) {
    console.log(`${val.id} ${val.websocket}`);
}

function findIdxOfUuid(uuid, arr) {
    for (let a = 0; a < arr.length; a++) if (arr[a].id == uuid) return a;
}

setInterval(disconnImposters, 6000);

function disconnImposters() {
    let tNow = new Date().getSeconds();
    impostorWs.forEach((wsCon, idx, arr) => {
        let tDiff = tNow - wsCon.tConn;
        if (wsCon.websocket.user) {
            impostorWs.splice(idx, 1);
            wsCon.websocket.send((new ReqStat('ok', `You were authorized.`)).toBeSent());
        }
        else if (tDiff > 6) {
            console.log(`Closing a connection.`);
            wsCon.websocket.close();
            impostorWs.splice(idx, 1);
        }
    });

}

/*
let example = {
    //Only required parameter is cred
    cred: {
        user: {name: 'username', uid: 12, uuid: '157fe1a1-31a3-4bbf-8c14-a14ffeb06684'},
        pres: {id: '10001'},
    },
    msg: {rec: [1, 3, 42], content: 'Hello, I've just joined. Can somebody, please tell me why are there so many people here?'},
    ctl: [
        {cmd: 'switch', param: 45},
        {cmd: 'kick', param: [1, 23, 34, 4, 21]}
}
*/

wsServer.on('connection', function connection(ws) {
    impostorWs.push({ tConn: (new Date()).getSeconds(), websocket: ws });
    ws.send((new ReqStat('ok', `You are now connected you have 6 seconds to authorize, otherwise your connection will be terminated.`)).toBeSent());

    ws.onmessage = (message) => {
        try {
            //Begin authentication
            if (!ws.user) {
                let msg = JSON.parse(message.data);
                if (msg.user && msg.pres) {
                    let status = Auth.ws(msg);
                    if (status.authorized) {
                        status.user.ws = ws;
                        ws.pres = status.pres;
                        ws.user = status.user
                        console.log(`${ws.user.uuid} was authorized in ${ws.pres.id} (${ws.pres.presName})`);
                    }
                    else throw new ReqStat('err', `You were not authorized. Either your credentials are wrong or the presentation doesn\'t exist.`, 'loud');
                }
                else throw new ReqStat('err', `You are not authorized because you did not provide credentials.`, 'loud');
            }
            //Successfully done authenticating

            //DO stuff user requested
            WsReq.do(ws, message.data);
            ws.send((new ReqStat('ok', ``)).toBeSent());
        }
        catch (err) {
            if (err instanceof ReqStat) ws.send(err.toBeSent());
            else console.log(`Something else went wrong ${JSON.stringify(err)}`)
        }
    };

    ws.on('close', function closing() {
        //ws.pres.removeUserByUid(ws.user.uid);
        //ws.pres.print();
    });
});

module.exports = wsServer;

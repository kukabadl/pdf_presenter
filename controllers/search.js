const Fuse = require('fuse.js')

var search = {
    options: {
        // isCaseSensitive: false,
        includeScore: true,
        // shouldSort: true,
        // includeMatches: false,
        // findAllMatches: false,
        // minMatchCharLength: 1,
        // location: 0,
        threshold: 0.6,

        // distance: 100,
        // useExtendedSearch: false,
        // ignoreLocation: false,
        // ignoreFieldNorm: false,
        keys: [
            "presName",
            "presId",
            "readableId",
            "hostName", 
            "desc"
        ]
    },

    initialized: false,

    init(dat){
        this.fuse = new Fuse(dat, this.options);
        this.initialized = true;
    },

    search(str) {
        return this.fuse.search(str);
    },

    add(dat) {
        this.fuse.add(dat);
    }
}

module.exports = search;
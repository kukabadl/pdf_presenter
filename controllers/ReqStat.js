class ReqStat{
    //Display options: ['silent', 'loud']
    constructor(code, msg = '', display = 'silent'){
        this.code = code;
        this.msg = msg;
        this.display = display;
    }
    toBeSent(){
        let temp = {status: {code: this.code, display: this.display, msg: this.msg}};
        return JSON.stringify(temp);
    }
}
module.exports = ReqStat;
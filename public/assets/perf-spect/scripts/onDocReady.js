var pres = null;
var ws = null;
var url;
function ready(callbackFunction) {
  if(document.readyState != 'loading') callbackFunction(event);
  else document.addEventListener("DOMContentLoaded", callbackFunction);
}
ready(event => {
  uiOnDocReady();
  init();
  pres = new canvasPresentation(url);
});
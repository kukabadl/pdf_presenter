class WsManager {
	constructor(cred) {
		this.cred = cred;
		this.ws = new WebSocket(`${(window.location.hostname == "localhost")? "ws" : "wss"}://${document.location.host}`);
		this.ws.manager = this;
		this.pending = [];
		this.ws.onopen = function (event) {
			//console.log(`Authorizing. (${JSON.stringify(cred)})`);
			this.send(JSON.stringify(cred));
			for(let a = 0; a < this.manager.pending.length; a++) {
				this.manager.sendMessage(this.manager.pending[a]);
			}
			this.manager.pending = [];
		}

		this.ws.onmessage = function (event) {
			var data = JSON.parse(event.data);
			//console.log(`Received ${event.data}`);
			if(data.status && data.status.code == 'err' && data.status.display == 'loud') {
				console.log(JSON.stringify(data));
			}
			if(data.msg) {
				showChatMessage('sent_by_them', data);
				//document.getElementById('message_box').innerHTML += `<div class="message sent_by_them">From: ${Data.msg.sender.uid}<br>${Data.msg.text}</div>`
			}
			if(data.ctl) {
				if(data.ctl.cmd == 'switch') {
					uiCheck(data.ctl.param, 'remote');
				}
				if(data.ctl.cmd == 'point') {
					let dat = data.ctl.param;
					//console.log('Got point ctl.');
					if(!presentedPointer.started) presentedPointer.start();
					presentedPointer.update(dat.t0, dat.time, dat.x, dat.y);
				}
				//document.getElementById('message_box').innerHTML += `<div class="message sent_by_them">From: ${Data.msg.sender.uid}<br>${Data.msg.text}</div>`
			}
			if(data.update) {
				
			}
		};

		this.ws.onerror = function (event) {

			//showChatMessage("<div class='error'>Problem due to some Error</div>");
		};

		this.ws.onclose = function (event) {
			console.log(`Connection was closed.`);
			//showChatMessage("<div class='chat-connection-ack'>Connection Closed</div>");
		};
	}

	sendMessage(msg = { msg: { rec: [0], content: 'Hello Oh, hello.' } }) {
		if(this.ws.readyState) {
			this.ws.send(JSON.stringify(msg));
			//console.log(`I\'ve just sent ${JSON.stringify(msg)}`);
		}
		else this.pending.push(msg);
	}

	sendToAll(message) {
		let msg_out = {
			sender: { name: ws.cred.user.name, uid: ws.cred.user.uid },
			msg: { rec: [0], content: message }
		}
		this.sendMessage(msg_out);
	}

	pageUpdate(page) {
		var event = { ctl: { cmd: 'switch', param: page } };
		//console.log(`Updating page: ${JSON.stringify(event)}`);
		this.ws.send(JSON.stringify(event));
	}

	pointUpdate(T0, TIME, X, Y) {
		let out ={
			ctl: {
				cmd: 'point',
				param: {
					t0: T0,
					time: TIME,
					x: X,
					y: Y
				}
			}
		}
		this.ws.send(JSON.stringify(out));
	}
}
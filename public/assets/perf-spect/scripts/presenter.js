
var pdfjsLib = window['pdfjs-dist/build/pdf'];
pdfjsLib.GlobalWorkerOptions.workerSrc = '/assets/pdfjs_essential/pdf.worker.js';
var t0, t1;
class canvasPresentation {
  constructor(url, startPage = NaN, stopPage = NaN, scale = 2.8, contId = 'the-canvas') {
    this.url = url;
    this.contId = contId;
    this.scale = scale;
    this.canvas = undefined;
    this.pdfDoc = undefined;
    this.ctx = undefined;
    this.pageRendering = undefined;
    this.pageNumPending = undefined;

    if(isNaN(startPage)) this.startPage = 1;
    else this.startPage = startPage;
    if(isNaN(stopPage)) this.stopPage = 1;
    this.page = startPage;
    this.presenterOnDocReady();
  }

  presenterOnDocReady() {
    this.pageRendering = false;
    this.pageNumPending = null;

    this.canvas = document.getElementById('the-canvas');
    this.ctx = this.canvas.getContext('2d');
    var ptr = this;

    //Asynchronously downloads PDF.
    pdfjsLib.getDocument({ url: this.url }).promise.then(pdf => {
      ptr.pdfDoc = pdf;
      ptr.stopPage = ptr.pdfDoc.numPages;
      ptr.queueRenderPage(ptr.startPage);
      setPageCount(ptr.stopPage);
      console.log("PDF was successfully loaded.");
      document.getElementById("pres-loading-spinner").classList.add("d-none");
      ptr.canvas.classList.remove("d-none");
      ptr.canvas.classList.add("d-block");
      uiCheck();
    });
  }

  queueRenderPage(num) {
    if(this.pageRendering) this.pageNumPending = num;
    else {
      this.renderPage(num);
    }
  }

  renderPage(num) {
    t0 = performance.now();
    this.pageRendering = true;
    this.page = num;

    var scale = this.scale;
    var contId = this.contId;
    var ctx = this.ctx;
    var ptr = this;


    // Using promise to fetch the page
    this.pdfDoc.getPage(num).then(page => {
      var viewport = page.getViewport({ scale: scale });
      var container = document.getElementById(contId);

      ptr.canvas.height = viewport.height;
      ptr.canvas.width = viewport.width;

      var renderContext = {
        canvasContext: ctx,
        viewport: viewport
      };

      page.render(renderContext).promise.then(() => {
        ptr.pageRendering = false;
        t1 = performance.now();
        //console.log(`Rendering took ${(t1 - t0) / 1000}s.`);

        if(ptr.pageNumPending !== null) {
          let pgNum = ptr.pageNumPending;
          // New page rendering is pending
          ptr.renderPage(ptr.pageNumPending);
          ptr.pageNumPending = null;
        }
      });
    });
  }

  lastPageIdx() {
    return this.stopPage;
  }

  pageNumOk(inp, isPush) {
    var temp = parseInt(inp);
    if(isPush === 'btn') temp += this.page;

    var out = { ok: false, first: false, last: false, okVal: this.page };
    if(temp >= this.startPage && temp <= this.stopPage) {
      out.ok = true;
      out.okVal = temp;
    }
    else out.okVal = this.page;

    if(out.okVal == this.startPage) out.first = true;
    else if(out.okVal == this.stopPage) out.last = true;

    return out;
  }
}





class svgPresentation {
  constructor(url, startPage = NaN, stopPage = NaN, scale = 1, contId = 'the-svg') {
    this.url = url;
    this.contId = contId;
    this.scale = scale;
    this.pageRendering = null;
    this.pageNumPending = null;

    if(isNaN(startPage)) this.startPage = 1;
    else this.startPage = startPage;
    if(isNaN(stopPage)) this.stopPage = 1;
    this.page = startPage;
    this.presenterOnDocReady();
  }

  renderPage(num) {
    this.page = num;
    //uiCheck(this.page);

    this.pageRendering = true;
    // Using promise to fetch the page
    var scale = this.scale;
    var contId = this.contId;
    this.pdfDoc.getPage(num).then(function (page) {
      var viewport = page.getViewport({ scale: scale });
      var container = document.getElementById(contId);

      container.style.width = '100vw';
      container.style.height = '100vh';

      container.setAttribute("style", "position: relative");

      page.getOperatorList().then(function (opList) {
        var svgGfx = new pdfjsLib.SVGGraphics(page.commonObjs, page.objs);
        return svgGfx.getSVG(opList, viewport);
      }).then(function (svg) {
        container.replaceChildren(svg);
      });
    });
  }

  queueRenderPage(num) {
    if(this.pageRendering && false) this.pageNumPending = num;
    else this.renderPage(num);
  }

  presenterOnDocReady() {
    this.pageRendering = false;
    this.pageNumPending = null;
    var ptr = this;
    //Asynchronously downloads PDF.
    pdfjsLib.getDocument(this.url).promise.then(function (pdfDoc_) {
      ptr.pdfDoc = pdfDoc_;
      // Initial/first page rendering
      ptr.pdfDoc = pdfDoc_;
      ptr.stopPage = ptr.pdfDoc.numPages;
      ptr.queueRenderPage(ptr.startPage);
      setPageCount(ptr.stopPage);
      console.log("PDF was successfully loaded.");
      uiCheck();
    });
  }

  lastPageIdx() {
    return this.stopPage;
  }

  pageNumOk(inp, isPush) {
    var temp = parseInt(inp);
    if(isPush) temp += this.page;

    var out = { ok: false, first: false, last: false, okVal: this.page };
    if(temp >= this.startPage && temp <= this.stopPage) {
      out.ok = true;
      out.okVal = temp;
    }
    else out.okVal = this.page;

    if(out.okVal == this.startPage) out.first = true;
    else if(out.okVal == this.stopPage) out.last = true;

    return out;
  }
}
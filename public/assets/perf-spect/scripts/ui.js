var live = false;
var chatSend, priv;

var presentedPointer = {
    init: function () {
        this.ptrElem = document.getElementById("canvas-pointer");
        this.canvElem = document.getElementById("the-canvas");
        this.timeOffset = 1000;
        this.ptrElem.style.display = "none";
    },
    update(t0, time, x, y) {
        t0 *= 10;
        let w = this.canvElem.getBoundingClientRect().width;
        let h = this.canvElem.getBoundingClientRect().height;

        let offsetTop = this.canvElem.offsetTop;
        let offsetLeft = this.canvElem.offsetLeft;

        let fromNow = t0 - new Date().getTime() + this.timeOffset;
        for(let i = 0; i < time.length; i++) {
            fromNow += time[i] * 10;
            if(fromNow > 0) {
                let posX = `${Math.round(w * x[i] / 1000 + offsetLeft)}px`;
                let posY = `${Math.round(h * y[i] / 1000 + offsetTop)}px`;
                setTimeout(this.timeout, fromNow, this.ptrElem, posX, posY);
            }
        }
    },

    timeout: function (el, x, y) {
        el.style.left = x;
        el.style.top = y;
    },

    stop: function () {
        this.started = false;
        this.ptrElem.style.display = "none";
    },

    start: function () {
        this.started = true;
        this.ptrElem.style.display = "inline-block";
    }

}

var sharePointer = {
    init: function () {
        this.t0 = Math.round(new Date().getTime() / 10);
        this.lastTime = this.t0;
        this.xTmp = undefined;
        this.yTmp = undefined;
        this.timeout = undefined;
        this.tmInt = 200;
        this.time = [];
        this.x = [];
        this.y = [];
    },

    rec: function (event) {
        let dims = event.srcElement.getBoundingClientRect();
        let now = Math.round(new Date().getTime() / 10);

        this.xTmp = Math.round(1000 * (event.clientX - dims.x) / dims.width);
        this.yTmp = Math.round(1000 * (event.clientY - dims.y) / dims.height);
        if(now - this.lastTime > 3 || Math.sqrt(this.xTmp ** 2 + this.yTmp ** 2) > 20) {
            this.time.push(now - this.lastTime);
            this.x.push(this.xTmp);
            this.y.push(this.yTmp);
            this.lastTime = now;
            if(this.timeout === undefined) this.timeout = setTimeout(this.send, this.tmInt, this);
        }
    },

    send: function (fakeThis) {
        //presentedPointer.update(fakeThis.t0, fakeThis.time, fakeThis.x, fakeThis.y);
        ws.pointUpdate(fakeThis.t0, fakeThis.time, fakeThis.x, fakeThis.y);
        fakeThis.init();
    }
};

function uiOnDocReady() {
    presentedPointer.init();
    updateUiBasedOnPrivileges();
    toggleSidepanel();
    document.getElementById('chat_composer').addEventListener("keydown", function (event) {
        if(event.which == 13) {
            event.preventDefault();
            chatSend();
        }
    });

    document.querySelector('body').addEventListener('fullscreenchange', (event) => {
        let fullScreenBtn = document.getElementById('fullscreen-btn');
        let panel = document.getElementById('controls-panel');
        let btns = document.querySelectorAll('.btn-change-on-fsc');
        if(document.fullscreenElement) {
            console.log(`Switched to fullscreen.`);
            fullScreenBtn.children[0].innerHTML = 'fullscreen_exit';

            //panel.classList.add('bg-secondary');
            //panel.classList.remove('bg-light');
            btns.forEach(btn => btn.classList.add('btn-light'));
        }
        else {
            fullScreenBtn.children[0].innerHTML = 'fullscreen';
            console.log(`Closed fullscreen.`);

            //panel.classList.add('bg-light');
            //panel.classList.remove('bg-secondary');
            btns.forEach(btn => btn.classList.remove('btn-light'));
            if(fullScreenBtn.classList.contains('active')) {
                fullScreenBtn.classList.remove('active');
                fullScreenBtn.setAttribute('aria-pressed', "false");
            }
        }
    });
}

function chBtns() {
    let targetState = Boolean(arguments[0]);
    for(let i = 1; i < arguments.length; i++) {
        arguments[i].disabled = !targetState;
        let parent = arguments[i].parentElement;
        if(targetState) {
            arguments[i].classList.remove("d-none");
            parent.classList.remove("d-none");
        }
        else {
            if(parent.childElementCount == parent.getElementsByClassName("d-none").length) {
                parent.classList.add("d-none");
            }
            arguments[i].classList.add("d-none");
        }
    }
}

function updateUiBasedOnPrivileges(prv = undefined) {
    let nonePrivileges = { chat: false, indivPace: { next: 0, prev: 0, custom: false }, control: [], role: undefined, voice: false };
    if(!prv) priv = nonePrivileges;
    else priv = prv;
    let tmp = document.getElementsByClassName("btn-dynamic");
    var btns = {};
    for(let i = 0; i < tmp.length; i++)
        btns[tmp[i].id] = tmp[i];
    chBtns(priv.chat, btns['chat_composer'], btns['chat-send-button'], btns['sidebar_collapse']);
    chBtns(priv.indivPace.custom, btns['next_page'], btns['prev_page']);
    chBtns((priv.control.includes('switch') || priv.control[0] === true), btns['go-live-btn']);
    chBtns((priv.control.includes('point') || priv.control[0] === true), btns['share-pointer-btn']);

    if(priv.indivPace.custom === true) {
        let cont = document.getElementById('pres_cont');
        cont.focus();
        cont.onkeydown = function (event) {
            if(event.which == 39) {
                event.preventDefault();
                uiCheck(1, 'btn')
            }
            else if(event.which == 37) {
                event.preventDefault();
                uiCheck(-1, 'btn')
            }
        }
    }
    //else document.getElementById('pres_cont').keydown = undefined;
    let pgNum = document.getElementById('page_num');
    pgNum.disabled = !priv.indivPace.custom;
}

function toggleSidepanel() {
    let btn = document.getElementById('sidebar_collapse');
    let btnIel = btn.querySelector("i");
    let btnSpanEl = btn.querySelector("span");

    let sidebar = document.getElementById('sidebar');
    if(sidebar.classList.contains('active')) {
        sidebar.classList.remove('active');
        btnIel.classList.add("backwards");
        btnIel.innerHTML = 'double_arrow';
        return false;
    }
    else {
        sidebar.classList.add('active');
        btnIel.classList.remove("backwards");
        btnIel.innerHTML = `chat`;
        return true;
    }
}

function uiCheck(inp, isPush) {
    var status = pres.pageNumOk(inp, isPush);
    if(priv.indivPace.custom) {
        if(status.first) {
            document.getElementById('prev_page').disabled = true;
            document.getElementById('next_page').disabled = false;
        }
        else if(status.last) {
            document.getElementById('next_page').disabled = true;
            document.getElementById('prev_page').disabled = false;
        }
        else {
            document.getElementById('prev_page').disabled = false;
            document.getElementById('next_page').disabled = false;
        }
    }

    if(status.ok) {
        pres.queueRenderPage(status.okVal);
        if(live && isPush != 'remote') {
            ws.pageUpdate(status.okVal);
        }
    }
    document.getElementById('page_num').value = status.okVal.toString();
}

function chatSend() {
    let txtArea = document.getElementById('chat_composer');
    let message = txtArea.value.trim();
    if(message != '') {
        ws.sendToAll(message);
        let msg = {
            msg: { text: message },
            sender: { name: 'Me', uid: ws.cred.user.uid }
        }
        showChatMessage('sent_by_me', msg);
    }
    txtArea.value = '';
}

function showChatMessage(cls, message) {
    chatElem = document.getElementById('message_box');
    chatElem.innerHTML += `<div class="message ${cls}">From: ${(message.sender.name) ? message.sender.name : message.sender.uid}<br>${message.msg.text}</div>`;

    let msgBox = document.getElementById("message_box");
    msgBox.scrollTop = msgBox.scrollHeight;
}


function toggleFullscreen() {
    if(document.fullscreenElement) {
        document.exitFullscreen().then(() => {
        }, (err) => {
            console.log(`Couldn't switch back from fullscreen. Probably already out.`);
        });
    }
    else {
        document.querySelector('body').requestFullscreen().then(() => {
        }, (err) => {
            console.log(`Couldn't switch to fullscreen. Probably already in.`);
        });
    }
}

function togglePointer(event) {
    let lv = (event.getAttribute("aria-pressed") === 'true');
    let canvas = document.getElementById("the-canvas");

    if(lv) {
        //presentedPointer.start();
        sharePointer.init();
        console.log('Pointer going online!');
        event.querySelector("i").innerHTML = 'auto_fix_normal';
        canvas.onmousemove = (ev) => sharePointer.rec(ev);

        canvas.classList.add('sharing-pointer');
        canvas.classList.remove('normal-pointer');
    }
    else {
        //presentedPointer.stop();
        console.log('Pointer going offline!');
        event.querySelector("i").innerHTML = 'auto_fix_off';
        canvas.onmousemove = undefined;

        canvas.classList.add('normal-pointer');
        canvas.classList.remove('sharing-pointer');
    }
}

function toggleLive(event) {
    live = (event.getAttribute("aria-pressed") === 'true');
    if(live) {
        ws.pageUpdate(pres.page);
        console.log('Going online!');
        event.querySelector("i").innerHTML = 'wifi_tethering';
    }
    else {
        console.log('Going offline!');
        event.querySelector("i").innerHTML = 'portable_wifi_off';
    }
}

function setPageCount(num) {
    document.getElementById('page_count').value = num;
}
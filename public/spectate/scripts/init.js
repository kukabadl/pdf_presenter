var url;

function init() {
    fetch(location.pathname.replace('spectate', 'auth')).then(res => {
        if(res.ok) return res.json();
        else throw new Error('Loading your credentials failed.');
    }).then(res => {
        updateUiBasedOnPrivileges(res.user.priv);
        sessionStorage.setItem('cred', JSON.stringify(res));
        ws = new WsManager(res);
    }).catch(err => {
        console.log(`${err}`);
    });
    url = location.href.replace('spectate', 'down');
}
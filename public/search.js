function ready(callbackFunction) {
    if (document.readyState != 'loading')
        callbackFunction();
    else
        document.addEventListener("DOMContentLoaded", callbackFunction);
}

ready(event => {
    document.getElementById("search-form").onkeyup = suggest;
});

let last = ""
function suggest(ev) {
    let str = ev.srcElement.value.trim();

    if(str == "" || str == last) return;
    last = str;

    const data = { search: str };
    let url = '/browse/suggest' + '?' + (new URLSearchParams(data)).toString();

    fetch(url, {
        method: 'GET',
        headers: { 'Content-Type': 'application/json' },
    }).then(response => {
        return response.json();
    }).then(dat => {
        console.log(dat)
        for (let a of dat) console.log(`Found "${a.item.presName}" by "${a.item.hostName}" with score ${a.score}`);
    }).catch((error) => {
        console.error('Error:', error);
    });
}
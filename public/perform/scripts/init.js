var url;

function init(){
    let res = JSON.parse(sessionStorage.getItem('cred'));
    let cred = { user: res.user, pres: res.pres };
    updateUiBasedOnPrivileges(cred.user.priv);
    url = location.href.replace('perform', 'down');
    ws = new WsManager(cred);
}
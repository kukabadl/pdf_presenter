
function ready(callbackFunction) {
  if (document.readyState != 'loading')
    callbackFunction();
  else
    document.addEventListener("DOMContentLoaded", callbackFunction);
}

/*
  Drag handler object to handle file selection by dragging
*/
var dragHandler = {
  init() {
    this.isFile = false;
    this.trackerEl = document.getElementById("file-drag-tracker");
    this.overlayEl = document.getElementById("drag-overlay");
    this.fileInputEl = document.getElementById("pdf-file");
  },
  enter(event) {
    if (event.dataTransfer.types) {
      for (var i = 0; i < event.dataTransfer.types.length; i++) {
        if (event.dataTransfer.types[i] == "Files") {
          this.isFile = true;
          this.trackerEl.style.display = "inline";
          this.overlayEl.style.display = "block";
          return;
        }
      }
    }
    this.isFile = false;
  },
  over(ev) {
    if (!this.isFile) return;
    ev.preventDefault();
    this.trackerEl.style.left = `${Math.round(ev.pageX - this.trackerEl.offsetWidth / 2)}px`;
    this.trackerEl.style.top = `${Math.round(ev.pageY - this.trackerEl.offsetHeight / 2)}px`;
  },
  drop(ev) {
    if (!this.isFile) return;
    this.leave(ev);
    ev.preventDefault();

    if (ev.dataTransfer.items && ev.dataTransfer.items.length === 1 && ev.dataTransfer.items[0].kind === 'file') {
      this.fileInputEl.files = ev.dataTransfer.files;
      inpValidator.isInvalid(this.fileInputEl);
    }
    else inpValidator.printErr(this.fileInputEl, `You can only upload one file`);
  },
  leave(ev) {
    this.trackerEl.style.display = "none";
    this.overlayEl.style.display = "none";
  }
}

function rollingCheck(ev) {
  inpValidator.isInvalid(ev.srcElement);
}

/*
  Input validator object takes care of input field validations
*/
var inpValidator = {
  init() {
    this.submitBtn = document.getElementById("edit-form-submit");
  },

  check() {
    let problems = document.getElementsByClassName("needs-validation").length;
    problems -= document.getElementsByClassName("is-valid").length;
    if (problems === 0) this.submitBtn.disabled = false;
    else this.submitBtn.disabled = true;
    return !Boolean(problems);
  },

  checkAll() {
    let all = document.getElementsByClassName("needs-validation");
    for (let i = 0; i < all.length; i++) {
      this.isInvalid(all[i]);
    }
    return this.check();
  },

  printErr(el, err) {
    el.parentElement.querySelector(".invalid-feedback").innerHTML = err;
    el.classList.add("is-invalid");
    el.classList.remove("is-valid");
    return true;
  },

  isInvalid(el) {
    if (el.type === 'file') {
      var filePath = el.value;
      var allowedExtensions = /(\.pdf)$/i;
      var feedbackEl = el.parentElement.querySelector(".invalid-feedback");
      if (allowedExtensions.exec(filePath)) {
        el.classList.add("is-valid");
        el.classList.remove("is-invalid");
      }
      else {
        el.classList.add("is-invalid");
        el.classList.remove("is-valid");
        if (filePath == "") feedbackEl.innerHTML = "Please select a pdf that you wish to present";
        else feedbackEl.innerHTML = "Only pdf presentations are supported";
      }
    }
    else if (el.type === 'text') {
      if (el.value.length >= 3 || (!el.required && el.value.length == 0)) {
        el.classList.add("is-valid");
        el.classList.remove("is-invalid");
      }
      else {
        el.classList.add("is-invalid");
        el.classList.remove("is-valid");
      }
    }
    this.check();
  }
}

/*
Function is called when the document is ready
*/
ready(event => {
  dragHandler.init();
  inpValidator.init();

  var form = document.getElementById("edit-form-form");
  form.querySelectorAll("input[type=text]").forEach(el => {
    el.addEventListener("blur", rollingCheck);
  });
  form.querySelectorAll("input[type=file]").forEach(el => {
    el.addEventListener("change", rollingCheck);
    //el.addEventListener("blur", rollingCheck);
  });

  form.onsubmit = function (event) {
    event.preventDefault();
    if (inpValidator.checkAll())
      var formData = new FormData(form);
    // Set up AJAX the request.
    var xhr = new XMLHttpRequest();
    xhr.open(form.getAttribute("method"), form.getAttribute("action"), true);
    // Set up a handler for when the task for the request is complete.
    xhr.onload = function () {
      if (xhr.status === 200) {
        let resp = JSON.parse(xhr.response);
        sessionStorage.setItem('cred', JSON.stringify(resp));

        //window.sessionStorage.setItem('pres', JSON.stringify(resp.pres));
        console.log(`Response: ${JSON.stringify(resp)}`);
        //window.sessionStorage.setItem('info', JSON.stringify(resp));
        location.replace(resp.url);
      }
      else {
        try {
          var resp = JSON.parse(xhr.response);
          console.log(`Error: ${JSON.stringify(resp)}`);
          //statusDiv.innerHTML = 'Upload failed<br>';
          for (const id in resp.err) {
            let element = document.getElementById(id);
            let parent = element.parentNode;

            element.classList.add("is-invalid");
            element.classList.remove("is-valid");
            console.log(parent.querySelector(".invalid-feedback"));
            parent.querySelector(".invalid-feedback").innerText = `${resp.err[id]}`;
          }
          inpValidator.check();
        }
        catch {
          console.log('Error occured during data upload');
        }
      }
    };
    xhr.send(formData);
  }
});
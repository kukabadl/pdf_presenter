//"include" all middleware
//const fileUpload = require('express-fileupload');
const cookieParser = require('cookie-parser');
const createError = require('http-errors');
const fileUpload = require('express-fileupload');

const express = require('express');
//const logger = require('morgan');
const path = require('path');
const pug = require('pug');

/*
  ws_manager.js exports a websocket server which handles all websocket traffic.
    When client first connects via websockets they have > 6 seconds to authenticate
    by sending their uuid which at that time must be registered under the presentation's
    "database" of users. Their websocket connection is then associated with their uuid.
*/
//var wsServer = require('./controllers/ws_manager.js');

var editRouter = require('./routes/edit');
var authRouter = require('./routes/auth');
var browseRouter = require('./routes/browse');
var performRouter = require('./routes/perform');
var spectateRouter = require('./routes/spectate');
var downRouter = require('./routes/down');

var app = express();

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'pug');

//app.use(logger('dev'));
//app.use(cookieParser());
app.use(express.json());

app.use(fileUpload({ createParentPath: true }));
app.use(express.static(path.join(__dirname, 'public')));

app.use('/edit', editRouter);
app.use('/down', downRouter);
app.use('/auth', authRouter);
app.use('/browse', browseRouter);
app.use('/perform', performRouter);
app.use('/spectate', spectateRouter);

//Catch 404 and forward to error handler
app.use(function (req, res, next) {
  next(createError(404));
});

//Error handler (Only in development)
app.use(function (err, req, res, next) {
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};

  // render the error page
  res.status(err.status || 500);
  res.render('error');
});

module.exports = app;

# PDF_presenter
## Instructions
In VSCode in preferences search for "html.format.wrapLineLength" and set it to 0
Then use Shift + Alt + F (Windows), Ctrl + Shift +I (Linux) To format text selection. This is Christmas. :D

## Application functionality
Application makes use of nginx server listening on port 443 for encrypted traffic to the website [try.labadum.eu](https://try.labadum.eu/index.html) but also for encrypted websocket communication (ws.labadum.eu).

After attempting to implement functionality of the server in django I settled for nodejs + expressjs server configuration.
Django didn't have good websocket support (at least from what I could find).
Most files are static. Rendering, fo those sites that need it, is done with pug middleware.

Client-side rendering of pdf files is done using Mozilla's [PDF.js](https://github.com/mozilla/pdf.js) library.

## Bugs
### Bug #3 [resolved]
<s>
- It works locally
- Access to filesystem is probably broken on Windows host. 
</s>
- There was indeed a problem, I was trying to save the files to another directory.
- I started using `path` to make the code platform independent.
- It seems to be working well even on Azure now.

### Bug #2 [resolved]
- Websockets are broken on linux azure apps
- [issue 49245](https://github.com/MicrosoftDocs/azure-docs/issues/49245),
- [issue 19578](https://github.com/MicrosoftDocs/azure-docs/issues/19578), 
- [issue 31771](https://github.com/MicrosoftDocs/azure-docs/issues/31771),
- [issue 10370](https://github.com/dotnet/aspnetcore/issues/10370),
- ÁÁÁÁÁAAAAAAáááááááaaaaaaa.......  
![](ws_on_azure.png)
- [Ws and http server on single](https://stackoverflow.com/questions/34808925/express-and-websocket-listening-on-the-same-port),
- [Ws auth on upgrade](https://www.npmjs.com/package/ws#client-authentication)

### Bug #1 [resolved]
- <s>In spectate, page renders sideways after an attempt to render a page while rendering is already in progress. Error:
"api.js:2665 Uncaught (in promise) Error: Cannot use the same canvas during multiple render() operations. Use different canvas or ensure previous operations were cancelled or completed. at InternalRenderTask.initializeGraphics (api.js:2665) at api.js:1105"</s>

## Roadmap
### Other
- come up with a reasonable name
    
### Changes to almost all pages
- add support for website translations
- <s>add favicons</s>

### Changes to [index](https://pdf-presenter.azurewebsites.net/edit)
- add pictures (also /images/chat.png does not exist)
- what to do with the carousel?
- get the rid of redundancy
- Search
    - come up with a way how search results could be displayed
    - Should index.html have that functionality?
    - Look at [fuse](https://www.npmjs.com/package/fuse.js/v/3.4.3) it could be a good starting point.
- <s>stretch Carousel pictures(on the top) to span as much space as possible</s>
- <s>unify the pictures inside featourettes(color-wise)</s>

### Create to [browse](https://pdf-presenter.azurewebsites.net/browse)
- page which previews currently running presentations
- should contain some filters
- this page shoud have search capabilities

### Changes to [edit](https://pdf-presenter.azurewebsites.net/edit)
- add some style and structure, pictures maybe?
- add some more functions to configure, options that depend on other selection 
- <s>we could use [floating labels](https://getbootstrap.com/docs/5.0/forms/floating-labels/#example) for text input fields, which would also make displaying errors very easy</s>
- <s>add file dropdown field so that users can drag'n'drop</s>
- <s>do the centering of the form properly (`<center><\center>`) tags are obsolete in html5)</s>
- <s>in javascript add mechanism to check if the file is indeed a .pdf and check the size</s>
- <s>add option to write errors directly to the input fields, not at the bottom</s>

### Changes to [perform](https://pdf-presenter.azurewebsites.net/perform)
- this page will also contain some added functionality, such as user control and dynamic changes to user privileges
- <s>for the most part `spectate` can be reused</s>
- <s>the style however should be a little different so that the presenter can distinguish between the spectate and perform views</s>

### Backend changes
- implement search
- Replace current websocket auth with auth on connection upgrade. That way it will be possible to get the rid of impostorWs.
- add support for custom ids, that could consist of alphanumerical characters (for public presentations only)
- add support for email communication and account creation
- implement some kind of expiration mechanism (delete presentations and users older than xxx days)
- add voice support
- <s>clean up</s>
- <s>public presentations do NOT work yet</s>
- <s>connecting to perform is broken</s>
- <s>implement websocket authentification, control and communication</s>

## Significant files and folders
### This project consists of many folders and files but only few are actually directy user-editable.
``` 
    ./app.js
        #basic configuration of the expressjs server
    ./bin
        #I don't even know why it is there. :D It contains some configuration which was generated automatically.
    ./controllers/
        #Custom scripts and classes that take care of some server functions or that are needed for other functions inside ./routes or app.js
    ./nginx/
        #This is just a copy of NGINX configuration files. In case of migration to other services.
    ./node_modules/
        #Folder with maaaaaany nodejs modules installed fore nodejs. No fun in there.
        #They are installed with nodejs package manager (npm): 'npm install <package_name> --save' command which is executed frome inside the root folder
    ./package-lock.json 
        #File maintained with npm. Track all installed packages and their versions.
    ./package,json
        #Configuration of npm.
    ./public/
        #This folder is the root folder of the server. You can access it from try.labadum.eu
        #It is the most important folder from the webdesign point of view. It contains all the static html files.
        #It's structure is:
        ./*.html
            #directly in this folder there are *.html files.
            #It is also home to the 'favicon.svg'
        ./images
            #Contains all images. 
            #If you want to include an image from your html file you will have to type './images/<image_name.type>'  
        ./scripts 
            #Contains all standalone scripts.
            #If you want to include a script from your html file you will have to type './scripts/<script_name.js>'  
        ./style
            #Contains all *.css files
            #If you want to include a script from your html file you will have to type './style/<stylesheet_name.js>'  
        ./routes
            #Contains scripts that take care of distributing work to server routines.
            #They route traffic to applications
        ./uploads   
            #Directory that stores user uploaded files.
        ./views
            #Directory containing files for dynamic rendering with pug engine.
``` 
## Folder tree
``` 
Project folder structure:
├── README.txt
├── app.js
├── bin
│   └── www
├── controllers
│   ├── auth.js
│   ├── presentation.js
│   ├── user.js
│   └── ws_manager.js
├── nginx
│   ├── try.labadum.eu
│   └── ws.labadum.eu
├── node_modules
│   ├── websocket
│   ├── widest-line
│   ├── window-size
│   ├── with
│   ├── wordwrap
│   ├── wrappy
│   ├── write-file-atomic
│   ├── ws
│   ├── xdg-basedir
│   ├── yaeti
│   └── ...
├── package-lock.json
├── package.json
├── public
│   ├── about.html          #[About us]. Available from the main page
│   ├── edit.html           #Page for setting up new presentations.
│   ├── favicon.svg         #The icon that shows in the left of the page tab.
│   ├── images              #Put all images here.
│   ├── index.html          #The first page user sees when visiting try.labadum.eu
│   ├── not_found.html      #Not found page. It is shown when the requested page is not found.
│   ├── pdfjs_essential     #Folder containing files needed for in-browser pdf rendering.
│   ├── scripts             #Folder containing *.js files used by the pages.
│   ├── spectate.html       #page that is used to view presentations. It is supposed only to be used by viewers.
│   |                           #Currently it is also used for performers. Not good.
|   ├── style               #Folder containing all stylesheets
│   └── upload.html         #This file is only temporary. Used for testing uploads.
├── routes
│   ├── edit.js
│   ├── perform.js
│   └── spectate.js
├── uploads
└── views
    ├── edit.pug
    ├── error.pug
    ├── layout.pug
    └── spectate.pug
``` 
